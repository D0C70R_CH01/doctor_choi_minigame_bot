/* CODE I3Y D0C70R_CH01 (https://gitlab.com/D0C70R_CH01) */
import discord from "discord.js";
import fs from "fs";
import consoleStamp from "console-stamp";
// import 끗

const __PREFIX: string = "?";
const __LOG_LEVEL: number = 2;
// 상수선언 끗

var _cs = consoleStamp(console, {format: ":date(HH:MM:ss.l) :label"});
var client: discord.Client = new discord.Client({intents: ["Guilds", "GuildMessages", "MessageContent", "GuildVoiceStates"]});
// 변수선언 끗

console.log("Hello,World!");
console.warn("Critical Error");
console.error("Ion Efflux");
// 헬로월드 발사
